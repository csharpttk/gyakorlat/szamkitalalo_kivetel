﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SzámKitaláló_Kivétel
{
    class Program
    {
       
            static void Main(string[] args)
            {               
                Random r=new Random();
                int szám = r.Next(1, 100);
                Console.WriteLine("Gondoltam egy számot 1 és 100 között...(Kérem a tippeket)");
                int tipp = -1;
                while (tipp != szám)
                {
                    try
                    {
                        tipp = Convert.ToInt16(Console.ReadLine());
                        //ha nem egész, FormatException típusú kivételt dob
                        if (tipp < 1 || tipp > 100)
                            throw new Egy_es_100Exception("Saját kivétel.. ");
                        if (tipp < szám) { Console.WriteLine("Túl kicsi"); }
                        if (tipp > szám) { Console.WriteLine("Túl nagy"); }
                    }
                    catch (Egy_es_100Exception ex)
                    {
                        Console.WriteLine(ex.Message+"Nem 1 és 100 közötti egész");
                    }
                    catch (FormatException ex)
                    {
                        Console.WriteLine("Nem egész");
                    }
                    catch
                    {
                        Console.WriteLine("Egyéb hiba");
                    }
                }
                Console.WriteLine("Gratulálok, eltalálta");
            }
        }
    }
