﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SzámKitaláló_Kivétel
{
    class Egy_es_100Exception: Exception
    {
        public Egy_es_100Exception(string message):base(message)
        {
        }
    }
}
